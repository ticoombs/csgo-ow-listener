import bz2
import os
import pyshark
import sys
import urllib3

# -----------------------------------------------
# EDIT THIS SECTION

CSGO_DIR = r""
NET_INTERFACE = ""

# EDIT THIS SECTION
# -----------------------------------------------


def get_demo(pkt):
    global PREV_DEM_FILE

    # get demo URL from pkt, download it, and extract it
    print("Found demo:")

    dem_file = ".".join(pkt.http.request_full_uri.split("/")[-1].split(".")[:-1])

    print("\tURL: " + pkt.http.request_full_uri)
    if os.path.exists(CSGO_DIR + dem_file):
        print("\tAlready downloaded, skipping...")
        return

    if PREV_DEM_FILE is not None and dem_file != PREV_DEM_FILE and os.path.exists(CSGO_DIR + PREV_DEM_FILE):
        print("\tRemoving previous demo file: " + PREV_DEM_FILE)
        os.unlink(CSGO_DIR + PREV_DEM_FILE)

    PROG_NUM_STEPS = 20
    PROG_FRMT = "\t\tprogress: [{:" + str(PROG_NUM_STEPS) + "}]"
    PROG_LEN = len(PROG_FRMT.format(" " * PROG_NUM_STEPS, ""))
    # stupid Windows console seems to have an error that grows with each leading \t,
    # so let's fudge the LEN a bit (printing too many \b chars doesn't matter)
    PROG_LEN *= 2

    # execute HTTP request
    r = http.request('GET', pkt.http.request_full_uri, preload_content=False)

    # set up parameters
    decomp = bz2.BZ2Decompressor()
    data_sz = 0
    data_ttl_sz = r.getheader('Content-Length')
    last_prog = 0
    prog_steps = 0
    if data_ttl_sz is not None:
        data_ttl_sz = int(data_ttl_sz)
        prog_step = round(data_ttl_sz / PROG_NUM_STEPS)
        data_ttl_sz_str = "{:.2f} MiB".format(data_ttl_sz / 1024 / 1024)
    else:
        prog_step = 1024 * 1024 * 2  # 2 MiB
        data_ttl_sz_str = "???"

    print("\tDownloading ({})...".format(data_ttl_sz_str))
    if data_ttl_sz is not None:
        print(PROG_FRMT.format(""), end="")
        sys.stdout.flush()

    # read in HTTP stream and decompress it on the fly
    with open(CSGO_DIR + dem_file, 'wb') as dem_out:
        for chunk in r.stream(1460):
            try:
                dem_out.write(decomp.decompress(chunk))
            except EOFError:
                print("\tERROR: BZ2'd demo file contains multiple streams... is this an OW demo?! Stopping...")
                return

            data_sz += len(chunk)
            last_prog += len(chunk)

            if last_prog >= prog_step:
                prog_steps += 1
                last_prog = 0
                if data_ttl_sz is not None:
                    print("\b" * PROG_LEN, end="")
                    print(PROG_FRMT.format("+" * min(prog_steps, PROG_NUM_STEPS)), end="")
                    sys.stdout.flush()

        if data_ttl_sz is not None:
            print("\b" * PROG_LEN, end="")
            print(PROG_FRMT.format("+" * PROG_NUM_STEPS))

            if data_sz < data_ttl_sz:
                print("\tWARNING: Downloaded {.2f} MiB of data (diff: {} bytes)".format(data_sz / 1024 / 1024,
                                                                                        data_sz - data_ttl_sz))
            else:
                PREV_DEM_FILE = dem_file

#    opener ="open" if sys.platform == "darwin" else "xdg-open"
#    subprocess.call([opener, CSGO_DIR + dem_file])
    print("\tDONE!")


# -----------------------------------------------


PREV_DEM_FILE = None

# Verify path to CSGO dir
if CSGO_DIR[-1] != os.sep:
    CSGO_DIR += os.sep
if not os.path.exists(CSGO_DIR + "serverconfig.vdf"):
    if os.path.isdir(CSGO_DIR + "csgo"):
        CSGO_DIR += "csgo" + os.sep

# Set up HTTP connection
http = urllib3.PoolManager(headers={'user-agent': 'Valve/Steam HTTP Client 3.0 (730)'})

# Capture packets
cap = pyshark.LiveCapture(interface=NET_INTERFACE,
                          bpf_filter="dst port 80",
                          display_filter="http.user_agent ~ \"Valve.+1\\.0\""
                                         " && http.host contains \"replay\"")

print("Listening for packets...")
cap.apply_on_packets(get_demo, timeout=None, packet_count=None)
