# CSGO Overwatch Demo Listener

Wanting to find out who you are /actually/ watching while checking that overwatch replay?

CSGO doesn't use encryption on their packets, so we can listen for the specific packet and download the real demo of the whole game, not just the part which the overwatch system gives you.
You can then integrate this into csgo-demo-manager or another application to parse the demo and check if the user already has been banned, etc.

## Usage:

```
git clone gitea.slowb.ro:ticoombs/csgo-ow-listener 
cd csgo-ow-listener
# edit the python file to include your CSGO location and your network interface
$EDITOR csgo_ow_listener.py
# now, as root run the app
# because we are listening to a network interface, so you need to be root!
sudo ./run
```

